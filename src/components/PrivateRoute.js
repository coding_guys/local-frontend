// @flow

import React from 'react';
import { Route } from 'react-router';

import storage from '../storage';
import { TOKEN_KEY } from '../api/auth';

type Props = {
  history: any
};

const PrivateRoute = (props: Props) => {
  if (!storage.get(TOKEN_KEY)) {
    props.history.push('/signin');
    return null;
  }

  return <Route {...props} />;
};

export default PrivateRoute;
