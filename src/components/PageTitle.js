// @flow

import styled from 'styled-components';

const PageTitle = styled.div`
  font-size: 24px;
  font-weight: bold;
  margin-bottom: 2rem;
`;

export default PageTitle;
