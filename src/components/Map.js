// @flow

import React, { Component } from 'react';
import Datamap from 'react-datamaps';
import { type Coordinates } from '../domain/packets';
import _ from 'lodash/fp';

export type Arc = {
  origin: Coordinates,
  destination: Coordinates,
  power: number,
  createdAt: number
};

type MapProps = {
  arcs: Array<Arc>
};

type Bubble = {
  radius: number,
  latitude: number,
  longitude: number
};

function powerToRadius(power: number): number {
  if (power > 10000) return 70;
  else if (power > 1000) return 50;
  else if (power > 750) return 40;
  else if (power > 500) return 30;
  else if (power > 100) return 20;
  else return 10;
}

class Map extends Component<MapProps> {
  shouldComponentUpdate(nextProps: MapProps) {
    return nextProps.arcs !== this.props.arcs;
  }

  render() {
    const bubbles: Array<Bubble> = _.map(
      arc => ({ ...arc.origin, radius: powerToRadius(arc.power), fillKey: 'bubble' }),
      this.props.arcs
    );

    return (
      <Datamap
        scope="world"
        fills={{
          defaultFill: '#abdda4',
          bubble: '#cd5c5c'
        }}
        bubbles={bubbles}
        arc={this.props.arcs}
        arcOptions={{
          strokeWidth: 1.5,
          arcSharpness: 1.5,
          animationSpeed: 300
        }}
      />
    );
  }
}

export default Map;
