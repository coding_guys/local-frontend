// @flow

import React from 'react';
import styled from 'styled-components';

const FormItemStyled = styled.div`
  display: block;
  position: relative;
  margin: 1rem;
`;

type Props = {
  children: any
};

const FormItem = (props: Props) => (
  <FormItemStyled {...props}>
    <div>
      <div>{props.children}</div>
    </div>
  </FormItemStyled>
);

export default FormItem;
