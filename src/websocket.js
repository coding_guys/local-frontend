// @flow

export function subscribe<T, K>(
  urlLocation: string,
  onMessage: T => void,
  onError: (K | string) => void,
  onClose?: () => void
): WebSocket {
  const url = `ws://localhost:8080/${urlLocation}`;

  let socket = new window.WebSocket(url);

  socket.onmessage = (event: MessageEvent) => {
    try {
      // $FlowFixMe
      const parsed = JSON.parse(event.data);
      onMessage(parsed);
    } catch (e) {
      onError(e);
    }
  };

  socket.onerror = onError;

  socket.onclose = e => {
    if (typeof onClose === 'function') {
      onClose();
    }
  };

  return socket;
}

export default { subscribe };
