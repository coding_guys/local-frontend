// @flow

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Layout as AntLayout, Menu, Icon } from 'antd';
import styled from 'styled-components';
import history from '../history';
import { withRouter } from 'react-router';
import { AttackConsumer } from '../core/attacks';
import { type Attack } from '../domain/attacks';

const { Header, Content, Sider } = AntLayout;

type LayoutProps = {
  children?: React$Node,
  location: {
    pathname: string
  }
};

type LayoutState = {
  collapsed: boolean
};

type AttackNotifierProps = {
  collapsed: boolean,
  newAttackOccurred: boolean,
  getAttacks: () => Promise<{ attacks: Array<Attack> }>
};

class AttackNotifier extends Component<AttackNotifierProps> {
  componentWillMount() {
    setInterval(this.props.getAttacks, 3000);
  }

  render() {
    return this.props.newAttackOccurred ? (
      <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
        <Icon type="warning" style={{ fontSize: '36px', marginBottom: '0.5rem', animation: 'pulse 2s infinite' }} />
        {!this.props.collapsed && <div style={{ fontSize: '14px' }}> You may be under attack! </div>}
      </div>
    ) : (
      <div />
    );
  }
}

class Layout extends Component<LayoutProps, LayoutState> {
  state = {
    collapsed: false
  };

  onCollapse = (collapsed: boolean) => {
    this.setState({ collapsed });
  };

  render() {
    return (
      <AntLayout style={{ height: '100%' }}>
        <Sider collapsible collapsed={this.state.collapsed} onCollapse={this.onCollapse}>
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              margin: '1rem',
              fontWeight: 'bold',
              fontSize: '16px',
              color: '#fff'
            }}
          >
            {this.state.collapsed ? 'AL' : 'Analysis Local'}
          </div>
          <Menu theme="dark" mode="inline" selectedKeys={[this.props.location.pathname]}>
            <Menu.Item key="/dashboard" style={{ marginTop: 0 }}>
              <Link to="dashboard">
                <Icon type="dashboard" />
                <span>Live Dashboard</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="/historical-data">
              <Link to="historical-data">
                <Icon type="pie-chart" />
                <span>Historical Data</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="/attacks">
              <Link to="attacks">
                <Icon type="bell" />
                <span>Attacks</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="/settings">
              <Link to="settings">
                <Icon type="setting" />
                <span>Settings</span>
              </Link>
            </Menu.Item>
          </Menu>
          <div
            style={{
              height: '50%',
              width: '100%',
              color: '#d9534f',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <AttackConsumer>
              {context => (
                <AttackNotifier
                  getAttacks={context.getAttacks}
                  newAttackOccurred={context.newAttackOccurred}
                  collapsed={this.state.collapsed}
                />
              )}
            </AttackConsumer>
          </div>
        </Sider>
        <AntLayout style={{ height: '100%' }}>
          <Header style={{ height: '5%', backgroundColor: '#f5f5f5' }}>
            <StyledLogout>
              <Icon
                type="logout"
                style={{ float: 'right', fontSize: '20px', cursor: 'pointer' }}
                onClick={() => history.push('/')}
              />
            </StyledLogout>
          </Header>
          <Content style={{ height: '90%' }}>{this.props.children}</Content>
        </AntLayout>
      </AntLayout>
    );
  }
}

const StyledLogout = styled.div`
  width: 100%;
  margin: 1rem 0 0 0;
`;

export default withRouter(Layout);
