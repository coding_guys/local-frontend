// @flow

export type AttackDetails = {
  attackType: string,
  initialAttackPower: number,
  peakAttackPower: number,
  attackDirection: string,
  attackProtocol: string,
  totalIncomingTraffic: number,
  totalOutgoingTraffic: number,
  totalIncomingPps: number,
  totalOutgoingPps: number,
  totalIncomingFlows: number,
  totalOutgoingFlows: number,
  averageIncomingTraffic: number,
  averageOutgoingTraffic: number,
  averageIncomingPps: number,
  averageOutgoingPps: number,
  averageIncomingFlows: number,
  averageOutgoingFlows: number,
  incomingIpFragmentedTraffic: number,
  outgoingIpFragmentedTraffic: number,
  incomingIpFragmentedPps: number,
  outgoingIpFragmentedPps: number,
  incomingTcpTraffic: number,
  outgoingTcpTraffic: number,
  incomingTcpPps: number,
  outgoingTcpPps: number,
  incomingSynTcpTraffic: number,
  outgoingSynTcpTraffic: number,
  incomingSynTcpPps: number,
  outgoingSynTcpPps: number,
  incomingUdptraffic: number,
  outgoingUdpTraffic: number,
  incomingUdpPps: number,
  outgoingUdpPps: number,
  incomingIcmpTraffic: number,
  outgoingIcmpTraffic: number,
  incomingIcmpPps: number,
  outgoingIcmpPps: number
};

export type AttackType = 'fastnetmon';

export type Attack = {
  attackType: AttackType,
  attack: {
    id: string,
    ip: string,
    attackDetails: AttackDetails,
    attackTimeEpochSeconds: number
  },
  finished: boolean
};
