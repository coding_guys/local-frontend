// @flow

export type Coordinates = {
  latitude: number,
  longitude: number
};

export type L3 = {
  source_addr: string,
  dst_addr: string,
  ip_protocol: number
};

export type L4Type = 'Icmp' | 'Tcp' | 'Udp' | 'Unknown';

export type L4 = {
  l4_type: L4Type,
  icmp_type: ?number,
  icmp_code: ?number,
  src_port: ?number,
  dst_port: ?number,
  ack: ?boolean,
  syn: ?boolean,
  rst: ?boolean,
  fin: ?boolean,
  window_size: ?number
};

export type L7Type = 'HttpRequest' | 'HttpResponse' | 'Unknown';

export type L7 = {
  packet_type: L7Type,
  method: ?string,
  path: ?string,
  host: ?string,
  status_code: ?number
};

export type PacketSample = {
  l3: L3,
  l4: L4,
  l7: L7,
  packet_size: number,
  sampling_rate: number,
  timestamp: number
};
