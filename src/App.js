// @flow

import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import Layout from './layout/Layout';
import Dashboard from './views/Dashboard';
import Settings from './views/Settings';
import SignIn from './views/SignIn';
import history from './history';
import { AttackProvider } from './core/attacks';
import { PacketSampleProvider } from './core/packets';
import { default as AttackReports } from './views/AttackReports';
import { default as HistoricalData } from './views/HistoricalData';

import './App.css';

const App = () => (
  <PacketSampleProvider>
    <AttackProvider>
      <Router history={history}>
        <Switch>
          <Route exact path="/" component={SignIn} />
          <Layout>
            <Switch>
              <Route exact path="/" component={Dashboard} />
              <Route exact path="/dashboard" component={Dashboard} />
              <Route exact path="/historical-data" component={HistoricalData} />
              <Route exact path="/attacks" component={AttackReports} />
              <Route exact path="/settings" component={Settings} />
            </Switch>
          </Layout>
        </Switch>
      </Router>
    </AttackProvider>
  </PacketSampleProvider>
);

export default App;
