open Antd;

let component = ReasonReact.statelessComponent("HistoricalData");

let dataSource = [|
  {"key": "a", "name": "test a"},
  {"key": "b", "name": "test b"},
|];

let columns = [|
  {
    "width": None,
    "title": "title",
    "key": "a",
    "render": Some((_, record) => ReasonReact.string(record##name)),
  },
|];

let make = _children => {
  ...component,
  render: _self =>
    <div style={ReactDOMRe.Style.make(~height="100%", ())}>
      <Table
        columns
        dataSource
        expandedRowRender={
          (record, _, _, _) =>
            <div>
              <h1> {ReasonReact.string("One")} </h1>
              <h2> {ReasonReact.string("Two")} </h2>
            </div>
        }
      />
    </div>,
};

let default = ReasonReact.wrapReasonForJs(~component, _props => make([||]));