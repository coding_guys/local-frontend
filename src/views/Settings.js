// @flow

import React, { Component } from 'react';
import { Input, Switch, Button } from 'antd';
import PageTitle from '../components/PageTitle';

import styled from 'styled-components';

class Settings extends Component<{}> {
  render() {
    return (
      <Container>
        <PageTitle>Settings</PageTitle>
        <LabeledInput style={{ marginBottom: '1rem' }}>
          <Label> E-mail: </Label>
          <Input />
        </LabeledInput>
        <div style={{ display: 'flex', width: '12rem', justifyContent: 'space-between' }}>
          <div style={{ fontWeight: 'bold' }}>Change password:</div>
          <Switch style={{ marginBottom: '1rem' }} />
        </div>
        <LabeledInput style={{ marginBottom: '0.5rem' }}>
          <Label style={{ width: '12rem' }}> Password: </Label>
          <Input type="password" />
        </LabeledInput>
        <LabeledInput style={{ marginBottom: '1rem' }}>
          <Label style={{ width: '12rem' }}> Repeat password: </Label>
          <Input type="password" />
        </LabeledInput>
        <Button type="primary" htmlType="submit" style={{ width: '5rem' }}>
          Save
        </Button>
      </Container>
    );
  }
}

const Label = styled.div`
  width: 8rem;
  font-weight: bold;
`;

const LabeledInput = styled.div`
  display: flex;
  align-items: center;
  width: 30rem;
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
  margin-left: 2rem;
`;

export default Settings;
