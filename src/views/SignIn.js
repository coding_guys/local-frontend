// @flow

import React, { Component } from 'react';
import { Icon, Button, Input, Row, Col } from 'antd';
import { Formik } from 'formik';
import yup from 'yup';
import styled from 'styled-components';
import FormItem from '../components/FormItem.js';
import { TOKEN_KEY, login } from '../api/auth.js';
import { Redirect } from 'react-router-dom';
import storage from '../storage.js';

const messages = {
  emailRequired: 'Email required!',
  passwordRequired: 'Password required!',
  invalidCredentials: 'Invalid credentials!',
  unexpectedError: 'Unexpected error occurred'
};

const validationSchema = yup.object().shape({
  password: yup.string().required(messages.passwordRequired),
  email: yup.string().required(messages.emailRequired)
});

const LogoTitle = styled.p`
  font-size: 2rem;
  color: black;
`;

const ErrorMessage = styled.p`
  color: red;
  text-align: left;
  margin-bottom: 0;
`;

const SignInForm = ({ values, errors, touched, handleSubmit, handleChange, handleBlur, isSubmitting }) => (
  <form onSubmit={handleSubmit}>
    <FormItem style={{ height: '2.5rem' }}>
      <Input
        name="email"
        value={values.email}
        onChange={handleChange}
        onBlur={handleBlur}
        prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
        placeholder="Email"
      />
      {errors.email && touched.email && <ErrorMessage>{errors.email}</ErrorMessage>}
    </FormItem>
    <FormItem style={{ height: '2.5rem' }}>
      <Input
        name="password"
        value={values.password}
        type="password"
        onChange={handleChange}
        onBlur={handleBlur}
        prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
        placeholder="Password"
      />
      {errors.password && touched.password && <ErrorMessage>{errors.password}</ErrorMessage>}
    </FormItem>
    <Button type="primary" htmlType="submit" loading={isSubmitting} style={{ width: '100%', marginTop: '1rem' }}>
      Sign In
    </Button>
  </form>
);

type State = {
  loggedIn: boolean
};

class SignIn extends Component<{}, State> {
  state = {
    loggedIn: false
  };

  render() {
    if (this.state.loggedIn) return <Redirect to={'/dashboard'} />;
    else {
      return (
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            height: '100%'
          }}
        >
          <Row style={{ textAlign: 'center' }}>
            <Col style={{ maxWidth: '300px' }}>
              <div>
                <LogoTitle>Analysis Local</LogoTitle>
              </div>
              <Formik
                initialValues={{
                  email: '',
                  password: ''
                }}
                validationSchema={validationSchema}
                onSubmit={(values, { setSubmitting, setErrors }) => {
                  login(values.email, values.password)
                    .then(res => {
                      setSubmitting(false);
                      storage.save(TOKEN_KEY, res.token);
                      this.setState({ loggedIn: true });
                    })
                    .catch(err => {
                      setSubmitting(false);
                      if (err.status === 401) {
                        setErrors({ password: messages.invalidCredentials });
                      } else {
                        setErrors({ password: messages.unexpectedError });
                      }
                    });
                }}
                render={SignInForm}
              />
            </Col>
          </Row>
        </div>
      );
    }
  }
}

export default SignIn;
