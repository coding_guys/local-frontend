// @flow

import React, { Component } from 'react';
import WS from '../websocket';
import _ from 'lodash/fp';
import moment from 'moment';
import styled from 'styled-components';
import Map from '../components/Map';
import { Pie, Line } from 'react-chartjs-2';
import { type Coordinates } from '../domain/packets';
import { type Arc } from '../components/Map';
import { randomId } from '../utils/commonUtils';
import { type PacketSample } from '../domain/packets';
import { notification } from 'antd';

const MAX_BATCHES = 49;
const ARCS_REFRESH_TIME = 1000;
const SOCKET_RECONNECTION_DELAY = 5000;

type Statistics = {
  packetsPerSecond: number,
  bytesPerSecond: number,
  time: string
};

type PacketGroup = {
  packets: Array<PacketSample>
};

type LocalizedPacketGroup = {
  coordinates: ?Coordinates,
  group: PacketGroup
};

type LocalizedPacketBatch = Array<LocalizedPacketGroup>;

type L4Distibution = {
  Tcp: number,
  Udp: number,
  Icmp: number,
  Unknown: number
};

function parseProtocolData(batches: Array<LocalizedPacketBatch>) {
  const empty: L4Distibution = {
    Tcp: 0,
    Udp: 0,
    Icmp: 0,
    Unknown: 0
  };

  const stats = _.reduce(
    (acc, batch) => {
      const packets: Array<PacketSample> = _.flatMap(localizedGroup => localizedGroup.group.packets, batch);
      const l4TypeCount: L4Distibution = _.reduce(
        (acc, packet) => _.update(packet.l4.l4_type, count => count + packet.sampling_rate, acc),
        empty,
        packets
      );

      return _.mergeWith((a, b) => a + b, acc, l4TypeCount);
    },
    empty,
    batches
  );

  return {
    labels: ['Tcp', 'Udp', 'Icmp', 'Unknown'],
    datasets: [
      {
        data: _.values(stats),
        backgroundColor: ['#F7464A', '#46BFBD', '#FDB45C', '#838383'],
        hoverBackgroundColor: ['#FF5A5E', '#5AD3D1', '#FFC870', '#a9a9a9']
      }
    ]
  };
}

function toArcs(packetGroups: Array<LocalizedPacketGroup>): Array<Arc> {
  return _.compact(
    _.map(localizedGroup => {
      if (localizedGroup.coordinates) {
        const packetCount = _.reduce((acc, p) => acc + p.sampling_rate, 0, localizedGroup.group.packets);

        return {
          origin: localizedGroup.coordinates,
          destination: {
            latitude: 50.049683,
            longitude: 19.944544
          },
          createdAt: Date.now(),
          power: packetCount
        };
      } else return null;
    }, packetGroups)
  );
}

function filterOutOldArcs(arcs: Array<Arc>) {
  const threshold = Date.now() - ARCS_REFRESH_TIME;
  return _.filter(arc => arc.createdAt > threshold, arcs);
}

function bytesChartData(currentStats) {
  return {
    labels: _.map(stats => moment(stats.time).format('mm:ss'), currentStats),
    datasets: [
      {
        fill: false,
        data: _.map(stats => stats.bytesPerSecond, currentStats)
      }
    ]
  };
}

function subscribeToSocket<T, K>(urlLocation: string, onMessage: T => void, onError: (K | string) => void) {
  WS.subscribe(urlLocation, onMessage, onError, () => {
    console.log(`${urlLocation} socket stopped! Restarting...`);
    setTimeout(() => subscribeToSocket(urlLocation, onMessage, onError), SOCKET_RECONNECTION_DELAY);
  });
}

type DashboardState = {
  batches: Array<LocalizedPacketBatch>,
  arcs: Array<Arc>,
  stats: Array<Statistics>,
  latestStats: {
    packetsPerSecond: number,
    bytesPerSecond: number
  },
  socketError: boolean
};

const initState: DashboardState = {
  batches: [],
  arcs: [],
  stats: [],
  latestStats: {
    packetsPerSecond: 0,
    bytesPerSecond: 0
  },
  socketError: false
};

class Dashboard extends Component<{}, DashboardState> {
  state = initState;

  onStatsUpdate = (stats: Statistics) => {
    console.log('STATS UPDATED');
    this.setState({
      socketError: false,
      stats: [..._.takeRight(9, this.state.stats), stats],
      latestStats: stats
    });
  };

  onNewBatch = (batch: LocalizedPacketBatch) => {
    const arcs = filterOutOldArcs([...toArcs(batch), ...this.state.arcs]);
    const batches = [batch, ..._.take(MAX_BATCHES, this.state.batches)];

    this.setState({ socketError: false, arcs, batches });
  };

  onSocketError = (_error: any) => {
    if (!this.state.socketError) {
      this.setState({ socketError: true });
      notification['error']({
        message: 'There was a network error!',
        duration: 0
      });
    }
  };

  componentWillMount() {
    // prettier-ignore
    subscribeToSocket("statistics-stream", this.onStatsUpdate, this.onSocketError);
    subscribeToSocket('aggregate-stream', this.onNewBatch, this.onSocketError);
  }

  render() {
    return (
      <Container style={{ height: '90%' }}>
        <LineChartTile title="BYTES PER SECOND" data={bytesChartData(this.state.stats)} />

        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <NumberTile title="PACKETS PER SECOND" value={this.state.latestStats.packetsPerSecond} />

          <NumberTile title="BYTES PER SECOND" value={this.state.latestStats.bytesPerSecond} />
        </div>

        <PieChartTile title="PROTOCOL L3" data={this.state.batches} />
        <MapTile arcs={this.state.arcs} />
        <LogsTile batches={this.state.batches} />
      </Container>
    );
  }
}

type NumberTileProps = {
  title: string,
  value: number
};

const NumberTile = (props: NumberTileProps) => (
  <Tile style={{ width: '195px', height: '195px' }}>
    <div className="tile-name">{props.title}</div>
    <div
      style={{
        fontWeight: 'bold',
        fontSize: '32px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%'
      }}
    >
      {props.value}
    </div>
  </Tile>
);

type LogsTileProps = {
  batches: Array<LocalizedPacketBatch>
};

class LogsTile extends Component<LogsTileProps> {
  shouldComponentUpdate(nextProps: LogsTileProps) {
    return nextProps.batches !== this.props.batches;
  }

  render() {
    return (
      <Tile style={{ width: '605px', height: '400px' }}>
        <div className="tile-name">LIVE LOGS</div>

        <LogContainer>
          <div style={{ overflow: 'scroll' }}>
            <table>
              <PacketLogsHeading>
                <tr>
                  <th>Latitude</th>
                  <th>Longitude</th>
                  <th>Packets</th>
                  <th>Bytes</th>
                  <th>Time</th>
                </tr>
              </PacketLogsHeading>
              <tbody>
                {_.flatten(this.props.batches).map(batch => {
                  const packet = _.first(batch.group.packets);
                  const totalPackets = _.sum(_.map(p => p.sampling_rate, batch.group.packets));
                  const totalBytes = _.sum(_.map(p => p.packet_size, batch.group.packets));

                  return (
                    <Packet key={randomId()}>
                      <td>{batch.coordinates ? batch.coordinates.latitude : 'unknown'}</td>
                      <td>{batch.coordinates ? batch.coordinates.longitude : 'unknown'}</td>
                      <td>{totalPackets}</td>
                      <td>{totalBytes}</td>
                      <td>{moment(packet.time).format('HH:mm:ss')}</td>
                    </Packet>
                  );
                })}
              </tbody>
            </table>
          </div>
        </LogContainer>
      </Tile>
    );
  }
}

const LineChartTile = props => (
  <Tile style={{ width: '750px', height: '400px' }}>
    <div className="tile-name" style={{ marginBottom: '10px' }}>
      {props.title}
    </div>
    <div className="tile-content" style={{ marginTop: '10px' }}>
      <Line
        data={props.data}
        options={{
          legend: {
            display: false
          },
          animation: false,
          responsive: true,
          datasetFill: false
        }}
        width={500}
        height={220}
        redraw
      />
    </div>
  </Tile>
);

type PieChartTileProps = {
  title: string,
  data: Array<LocalizedPacketBatch>
};

const PieChartTile = (props: PieChartTileProps) => (
  <Tile style={{ width: '400px', height: '400px' }}>
    <div className="tile-name" style={{ marginBottom: '10px' }}>
      {props.title}
    </div>
    <div className="tile-content">
      <Pie data={parseProtocolData(props.data)} width={400} height={300} />
    </div>
  </Tile>
);

const MapTile = (props: { arcs: Array<Arc> }) => (
  <Tile style={{ width: '750px', height: '400px' }}>
    <div className="tile-name">MAP</div>
    <Map arcs={props.arcs} />
  </Tile>
);

const Container = styled.div`
  background-color: #f5f5f5;
  height: 100%;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  align-items: center;
`;

const Tile = styled.div`
  margin: 5px;
  background-color: #fff;
  padding: 20px;
  box-shadow: rgba(0, 0, 0, 0.16) 0px 5px 8px 0px;
  font-size: 10px;
  transition: 0s 1000s;
  display: block;

  .tile-name {
    margin: 0;
    width: 100%;
    font-weight: bold;
  }

  .tile-content {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
  }
`;

const LogContainer = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  height: 90%;
  margin: 10px 0px;
  overflow: scroll;
`;

const PacketLogsHeading = styled.thead`
  width: 100%;
  margin: 0.5rem;

  th {
    padding: 0 1.5rem 0 1.5rem;
    text-align: left;
  }
`;

const Packet = styled.tr`
  width: 100%;
  margin: 0.5rem;

  td {
    padding: 0 1.5rem 0 1.5rem;
    text-align: left;
  }
`;

export default Dashboard;
