// @flow

import React, { Component } from 'react';
import { Table, Select, Input, Button, InputNumber } from 'antd';
import moment from 'moment';
import styled from 'styled-components';
import _ from 'lodash/fp';
import { type PacketSample } from '../domain/packets';
import { PacketSampleConsumer } from '../core/packets';
import { type Field } from '../utils/commonTypes';
import { type PaginatedResponse } from '../utils/commonTypes';
import { randomId } from '../utils/commonUtils';
import { type PacketSampleQuery } from '../api/packets';
import PageTitle from '../components/PageTitle';

const { Option } = Select;

const ENTITIES_PER_PAGE = 10;

const columns = [
  {
    title: 'source',
    dataIndex: 'l3.source_addr',
    key: 'l3.source_addr'
  },
  {
    title: 'destination',
    dataIndex: 'l3.dst_addr',
    key: 'l3.dst_addr'
  },
  {
    title: 'packet size',
    dataIndex: 'packet_size',
    key: 'packet_size'
  },
  {
    title: 'sampling rate',
    dataIndex: 'sampling_rate',
    key: 'sampling_rate'
  },
  {
    title: 'time',
    dataIndex: 'time',
    key: 'time'
  }
];

type SectionProps = {
  sectionName: string,
  fields: Array<Field>,
  nameWidth: string
};

const Section = (props: SectionProps) => {
  return (
    <div style={{ marginBottom: '1rem' }}>
      <table>
        <thead>
          <tr>
            <td>
              <b>{props.sectionName}</b>
            </td>
          </tr>
        </thead>
        <tbody>
          {props.fields.map(field => (
            <tr key={field.name}>
              <td style={{ width: props.nameWidth }}>{field.name}</td>
              <td>{field.value === null || field.value === undefined ? '-' : String(field.value)}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

function process(sample: PacketSample) {
  const id = randomId();
  const time = moment(sample.timestamp).format('HH:mm:ss DD/MM/YYYY');
  return { key: id, ...sample, time };
}

const PacketSampleView = (props: PacketSample) => {
  return (
    <div style={{ width: '70%', display: 'flex', justifyContent: 'space-between' }}>
      <div>
        <Section
          sectionName="Layer 3"
          nameWidth="10rem"
          fields={[
            {
              name: 'Source address',
              value: props.l3.source_addr
            },
            {
              name: 'Destination address',
              value: props.l3.dst_addr
            },
            {
              name: 'IP protocol',
              value: props.l3.ip_protocol
            }
          ]}
        />
        <Section
          sectionName="Layer 4"
          nameWidth="10rem"
          fields={[
            {
              name: 'Type',
              value: props.l4.l4_type
            },
            {
              name: 'Icmp Type',
              value: props.l4.icmp_type
            },
            {
              name: 'Icmp Code',
              value: props.l4.icmp_code
            },
            {
              name: 'Source Port',
              value: props.l4.src_port
            },
            {
              name: 'Destination Port',
              value: props.l4.dst_port
            },
            {
              name: 'Ack flag',
              value: props.l4.ack
            },
            {
              name: 'Syn flag',
              value: props.l4.syn
            },
            {
              name: 'Rst flag',
              value: props.l4.rst
            },
            {
              name: 'Fin flag',
              value: props.l4.fin
            },
            {
              name: 'Window size',
              value: props.l4.window_size
            }
          ]}
        />
      </div>

      {props.l7 ? (
        <Section
          sectionName="Layer 7"
          nameWidth="10rem"
          fields={[
            {
              name: 'Type',
              value: props.l7.packet_type
            },
            {
              name: 'Method',
              value: props.l7.method
            },
            {
              name: 'Path',
              value: props.l7.path
            },
            {
              name: 'Host',
              value: props.l7.host
            },
            {
              name: 'Status Code',
              value: props.l7.status_code
            }
          ]}
        />
      ) : (
        <div style={{ fontWeight: 'bold' }}>No Layer 7 info </div>
      )}
    </div>
  );
};

type HistoricalDataTableProps = {
  page: number,
  packetSamples: PaginatedResponse<PacketSample>,
  setPage: number => void
};

class HistoricalDataTable extends Component<HistoricalDataTableProps, {}> {
  onChange = (e: { current: number }) => {
    this.props.setPage(e.current);
  };

  render() {
    const dataSource = _.map(process, this.props.packetSamples.entities);

    return (
      <Table
        columns={columns}
        dataSource={dataSource}
        expandedRowRender={record => <PacketSampleView {...record} />}
        pagination={{
          current: this.props.page,
          pageSize: ENTITIES_PER_PAGE,
          total: this.props.packetSamples.pageCount
        }}
        onChange={this.onChange}
      />
    );
  }
}

const Text = styled.div`
  display: flex;
  align-items: center;
  margin-right: 1.5rem;
  font-weight: bold;
`;

type FilterInputProps = {
  name: string,
  setter: (?string) => void
};

const FilterInput = (props: FilterInputProps) => (
  <div style={{ display: 'flex', marginBottom: '0.5rem' }}>
    <Text style={{ marginRight: '0.5rem' }}>{props.name}:</Text>
    <Input style={{ width: '10rem' }} onChange={e => props.setter(e.target.value)} />
  </div>
);

type FilterSelectProps = {
  name: string,
  options: Array<string>,
  setter: (?string) => void,
  allowClear: ?boolean
};

const FilterSelect = (props: FilterSelectProps) => (
  <div style={{ display: 'flex', marginBottom: '0.5rem' }}>
    <Text style={{ marginRight: '0.5rem' }}>{props.name}:</Text>
    <Select style={{ width: '10rem' }} onChange={props.setter} allowClear={props.allowClear}>
      {props.options.map(option => (
        <Option key={option} value={option}>
          {option}
        </Option>
      ))}
    </Select>
  </div>
);

type FromToProps = {
  name: string,
  setFrom: (?number) => void,
  setTo: (?number) => void
};

const FromTo = (props: FromToProps) => (
  <div style={{ display: 'flex', marginBottom: '0.5rem' }}>
    <Text style={{ marginRight: '0.5rem' }}>{props.name}:</Text>
    <div style={{ display: 'flex', alignItems: 'center' }}>
      <InputNumber style={{ width: '4rem', marginRight: '0.5rem' }} onChange={props.setFrom} />
      {'-'}
      <InputNumber style={{ width: '4rem', marginLeft: '0.5rem' }} onChange={props.setTo} />
    </div>
  </div>
);

type TableFiltersProps = {
  setSource: (?string) => void,
  setDestination: (?string) => void,
  setL4Type: (?string) => void,
  setL7Type: (?string) => void,
  setPacketSizeFrom: (?number) => void,
  setPacketSizeTo: (?number) => void,
  onSubmit: () => void
};

const TableFilters = (props: TableFiltersProps) => {
  return (
    <div style={{ width: '90%', marginBottom: '2rem' }}>
      <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
        <TableRow>
          <FilterInput name="Source" setter={props.setSource} />
          <FilterInput name="Destination" setter={props.setDestination} />
          <FilterSelect
            name="Layer 4"
            allowClear
            options={['Tcp', 'Udp', 'Icmp', 'Unknown']}
            setter={props.setL4Type}
          />
          <FilterSelect
            name="Layer 7"
            allowClear
            options={['HttpRequest', 'HttpResponse', 'Unknown']}
            setter={props.setL7Type}
          />
          <FromTo name="Packet size" setFrom={props.setPacketSizeFrom} setTo={props.setPacketSizeTo} />
        </TableRow>
        <Button style={{ width: '8rem' }} type="primary" onClick={props.onSubmit}>
          Search
        </Button>
      </div>
    </div>
  );
};

type HistoricalDataProps = {
  packetSamples: PaginatedResponse<PacketSample>,
  getPacketSamples: PacketSampleQuery => Promise<{ packetSamples: PaginatedResponse<PacketSample> }>
};

type HistoricalDataState = {
  filters: {
    l4Type: ?string,
    l7Type: ?string,
    source: ?string,
    destination: ?string,
    packetSizeFrom: ?number,
    packetSizeTo: ?number
  },
  page: number
};

class HistoricalData extends Component<HistoricalDataProps, HistoricalDataState> {
  state = {
    filters: {
      l4Type: null,
      l7Type: null,
      source: null,
      destination: null,
      packetSizeFrom: null,
      packetSizeTo: null
    },
    page: 1
  };

  componentDidMount() {
    this.getPackets();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.page !== this.state.page) {
      this.getPackets();
    }
  }

  getPackets = () => {
    const query = { ...this.state.filters, page: this.state.page, entitiesPerPage: ENTITIES_PER_PAGE };
    const queryWithoutEmpty = _.mapValues(v => (v === '' ? null : v), query);
    this.props.getPacketSamples(queryWithoutEmpty);
  };

  render() {
    return (
      <Container>
        <PageTitle>Historical data</PageTitle>
        <TableFilters
          setL4Type={l4Type => this.setState(_.set('filters.l4Type', l4Type))}
          setL7Type={l7Type => this.setState(_.set('filters.l7Type', l7Type))}
          setSource={source => this.setState(_.set('filters.source', source))}
          setDestination={destination => this.setState(_.set('filters.destination', destination))}
          setPacketSizeFrom={packetSizeFrom => this.setState(_.set('filters.packetSizeFrom', packetSizeFrom))}
          setPacketSizeTo={packetSizeTo => this.setState(_.set('filters.packetSizeTo', packetSizeTo))}
          onSubmit={() => this.getPackets()}
        />
        <div style={{ width: '90%', height: '90%' }}>
          <HistoricalDataTable
            page={this.state.page}
            packetSamples={this.props.packetSamples}
            setPage={page => this.setState({ page })}
          />
        </div>
      </Container>
    );
  }
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
  margin-left: 2rem;
`;

const TableRow = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;

  align-items: center;

  .ant-input,
  .ant-select {
    margin-right: 2rem;
  }
`;

function withConsumer(WrapperComponent) {
  return class extends Component<{}> {
    render() {
      return (
        <PacketSampleConsumer>
          {context => (
            <HistoricalData packetSamples={context.packetSamples} getPacketSamples={context.getPacketSamples} />
          )}
        </PacketSampleConsumer>
      );
    }
  };
}

export default withConsumer(HistoricalData);
