// @flow

import React, { Component } from 'react';
import { type Attack } from '../domain/attacks';
import { Table } from 'antd';
import { AttackConsumer } from '../core/attacks';
import moment from 'moment';
import styled from 'styled-components';
import _ from 'lodash/fp';
import { type Field } from '../utils/commonTypes';
import PageTitle from '../components/PageTitle';

type AttackViewColumnProps = {
  fields: Array<Field>,
  nameWidth: string
};

const AttackViewColumn = (props: AttackViewColumnProps) => {
  return (
    <div>
      {props.fields.map(field => (
        <tr>
          <td style={{ width: props.nameWidth }}>
            <b>{field.name}</b>
          </td>
          <td>{field.value}</td>
        </tr>
      ))}
    </div>
  );
};

const AttackView = (props: Attack) => {
  const details = props.attack.attackDetails;

  return (
    <div style={{ width: '70%', display: 'flex', justifyContent: 'space-between' }}>
      <AttackViewColumn
        nameWidth="15rem"
        fields={[
          {
            name: 'Attack type',
            value: details.attackType
          },
          {
            name: 'Initial attack power',
            value: details.initialAttackPower
          },
          {
            name: 'Peak attack power',
            value: details.peakAttackPower
          },
          {
            name: 'Attack direction',
            value: details.attackDirection
          },
          {
            name: 'Attack protocol',
            value: details.attackProtocol
          },
          {
            name: 'Total incoming traffic',
            value: details.totalIncomingTraffic
          },
          {
            name: 'Total outgoing traffic',
            value: details.totalOutgoingTraffic
          },
          {
            name: 'Total incoming pps',
            value: details.totalIncomingPps
          },
          {
            name: 'Total outgoing pps',
            value: details.totalOutgoingPps
          },
          {
            name: 'Total incoming flows',
            value: details.totalIncomingFlows
          },
          {
            name: 'Total outgoing flows',
            value: details.totalOutgoingFlows
          },
          {
            name: 'Average incoming traffic',
            value: details.averageIncomingTraffic
          },
          {
            name: 'Average outgoing traffic',
            value: details.averageOutgoingTraffic
          },
          {
            name: 'Average incoming pps',
            value: details.averageIncomingPps
          },
          {
            name: 'Average outgoing pps',
            value: details.averageOutgoingPps
          },
          {
            name: 'Average incoming flows',
            value: details.averageIncomingFlows
          },
          {
            name: 'Average outgoing flows',
            value: details.averageOutgoingFlows
          },
          {
            name: 'Incoming ip fragmented traffic',
            value: details.incomingIpFragmentedTraffic
          },
          {
            name: 'Outgoing ip fragmented traffic',
            value: details.outgoingIpFragmentedTraffic
          }
        ]}
      />
      <AttackViewColumn
        nameWidth="15rem"
        fields={[
          {
            name: 'Incoming ip fragmented pps',
            value: details.incomingIpFragmentedPps
          },
          {
            name: 'Outgoing ip fragmented pps',
            value: details.outgoingIpFragmentedPps
          },
          {
            name: 'Incoming tcp traffic',
            value: details.incomingTcpTraffic
          },
          {
            name: 'Outgoing tcp pps',
            value: details.outgoingTcpTraffic
          },
          {
            name: 'Incoming tcp pps',
            value: details.incomingTcpPps
          },
          {
            name: 'Outgoing tcp pps',
            value: details.outgoingTcpPps
          },
          {
            name: 'Incoming syn tcp traffic',
            value: details.incomingSynTcpTraffic
          },
          {
            name: 'Outgoing syn tcp traffic',
            value: details.outgoingSynTcpTraffic
          },
          {
            name: 'Incoming syn tcp pps',
            value: details.incomingSynTcpPps
          },
          {
            name: 'Outgoing syn tcp pps',
            value: details.outgoingSynTcpPps
          },
          {
            name: 'Incoming udp traffic',
            value: details.incomingUdptraffic
          },
          {
            name: 'Outgoing udp pps',
            value: details.outgoingUdpTraffic
          },
          {
            name: 'Incoming udp pps',
            value: details.incomingUdpPps
          },
          {
            name: 'Outgoing udp pps',
            value: details.outgoingUdpPps
          },
          {
            name: 'Incoming icmp traffic',
            value: details.incomingIcmpTraffic
          },
          {
            name: 'Outgoing icmp pps',
            value: details.outgoingIcmpTraffic
          },
          {
            name: 'Incoming icmp pps',
            value: details.incomingIcmpPps
          },
          {
            name: 'Outgoing icmp pps',
            value: details.outgoingIcmpPps
          }
        ]}
      />
    </div>
  );
};

const columns = [
  {
    title: 'id',
    dataIndex: 'id',
    key: 'id'
  },
  {
    title: 'type',
    dataIndex: 'attackDetails.attackType',
    key: 'attackDetails.attackType'
  },
  {
    title: 'time',
    dataIndex: 'time',
    key: 'time'
  }
];

function process(attack: Attack) {
  const date = new Date(attack.attack.attackTimeEpochSeconds * 1000);
  const time = moment(date).format('HH:mm:ss DD/MM/YYYY');
  return { ...attack, time };
}

type AttackTableProps = {
  attacks: Array<Attack>,
  getAttacks: (forceUpdate?: boolean) => Promise<{ attacks: Array<Attack> }>
};

class AttacksTable extends Component<AttackTableProps> {
  componentDidMount() {
    this.props.getAttacks();
  }

  render() {
    const dataSource = _.map(process, this.props.attacks);
    return <Table columns={columns} dataSource={dataSource} expandedRowRender={record => <AttackView {...record} />} />;
  }
}

class AttackReports extends Component<{}> {
  render() {
    return (
      <Container>
        <PageTitle>Attack reports</PageTitle>
        <div style={{ width: '90%', height: '90%' }}>
          <AttackConsumer>
            {context => <AttacksTable attacks={context.attacks} getAttacks={context.getAttacks} />}
          </AttackConsumer>
        </div>
      </Container>
    );
  }
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
  margin-left: 2rem;
`;

export default AttackReports;
