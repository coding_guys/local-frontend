//@flow
import React, { Component } from 'react';

import { type Attack } from '../../domain/attacks';
import { getAttacks } from '../../api/attacks';
import _ from 'lodash/fp';

const NEW_ATTACK_THRESHOLD = 5 * 60;

export type AttacksInjectedProps = {
  attacks: Array<Attack>,
  newAttackOccurred: boolean,
  getAttacks: () => Promise<{ attacks: Array<Attack> }>
};

function getAttacksState() {
  return {
    attacks: [],
    newAttackOccurred: false,
    getAttacks: () => Promise.reject()
  };
}

const { Provider, Consumer } = React.createContext(getAttacksState());

export const AttackConsumer = Consumer;

type Props = { children: React$Node };

function isNew(attack: Attack): boolean {
  const now = new Date().getTime() / 1000;
  return now - attack.attackTimeEpochSeconds < NEW_ATTACK_THRESHOLD;
}

export class AttackProvider extends Component<Props, AttacksInjectedProps> {
  constructor() {
    super();
    this.state = {
      attacks: [],
      newAttackOccurred: false,
      getAttacks: this.getAttacks
    };
  }

  getAttacks = (): Promise<{ attacks: Array<Attack> }> => {
    return getAttacks().then(attacks => {
      const newAttackOccurred = _.some(isNew, attacks);

      this.setState({ attacks, newAttackOccurred });
      return { attacks };
    });
  };

  render() {
    return <Provider value={this.state}> {this.props.children} </Provider>;
  }
}
