// @flow

import React, { Component } from 'react';

type ProviderState = {
  networkFailed: boolean
};

const { Provider, Consumer } = React.createContext({ networkFailed: false });

export const StatusConsumer = Consumer;

type ProviderProps = { children: React$Node };

export class StatusProvider extends Component<ProviderProps, ProviderState> {
  state = { networkFailed: false };

  render() {
    return <Provider value={this.state}> {this.props.children} </Provider>;
  }
}
