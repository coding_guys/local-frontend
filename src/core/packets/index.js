//@flow
import React, { Component } from 'react';

import { type PacketSample } from '../../domain/packets';
import { getPacketSamples } from '../../api/packets';
import { type PaginatedResponse } from '../../utils/commonTypes';
import { type PacketSampleQuery } from '../../api/packets';

export type PacketSamplesInjectedProps = {
  packetSamples: PaginatedResponse<PacketSample>,
  getPacketSamples: PacketSampleQuery => Promise<{ packetSamples: PaginatedResponse<PacketSample> }>
};

function getPacketSamplesState() {
  return {
    packetSamples: { pageCount: 0, entities: [] },
    getPacketSamples: () => Promise.reject()
  };
}

const { Provider, Consumer } = React.createContext(getPacketSamplesState());

export const PacketSampleConsumer = Consumer;

type Props = { children: React$Node };

export class PacketSampleProvider extends Component<Props, PacketSamplesInjectedProps> {
  constructor() {
    super();
    this.state = {
      packetSamples: { pageCount: 0, entities: [] },
      getPacketSamples: this.getPacketSamples
    };
  }

  getPacketSamples = (query: PacketSampleQuery): Promise<{ packetSamples: PaginatedResponse<PacketSample> }> => {
    return getPacketSamples(query).then(packetSamples => {
      this.setState({ packetSamples });
      return { packetSamples };
    });
  };

  render() {
    return <Provider value={this.state}> {this.props.children} </Provider>;
  }
}
