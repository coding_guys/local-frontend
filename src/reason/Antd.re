open Belt;

module Input = {
  [@bs.module "antd"] external component: ReasonReact.reactClass = "Input";

  [@bs.deriving abstract]
  type props = {
    id: Js.Nullable.t(string),
    value: Js.Nullable.t(string),
    style: Js.Nullable.t(ReactDOMRe.style),
    onChange: Js.Nullable.t(ReactEvent.Form.t => unit),
  };

  let make = (~id=?, ~value=?, ~style=?, ~onChange=?, children) => {
    let props =
      props(
        ~id=Js.Nullable.fromOption(id),
        ~value=Js.Nullable.fromOption(value),
        ~style=Js.Nullable.fromOption(style),
        ~onChange=Js.Nullable.fromOption(onChange),
      );
    ReasonReact.wrapJsForReason(~reactClass=component, ~props, children);
  };
};

module Table = {
  [@bs.module "antd"] external component: ReasonReact.reactClass = "Table";

  type column('a) = {
    .
    "title": string,
    "key": string,
    "dataIndex": string,
    "width": Js.Nullable.t(int),
    "render": Js.Nullable.t((string, 'a) => ReasonReact.reactElement),
  };

  [@bs.deriving abstract]
  type props('a) = {
    dataSource: array('a),
    columns: array(column('a)),
    rowKey: Js.Nullable.t(string),
    expandedRowRender:
      Js.Nullable.t(('a, int, int, bool) => ReasonReact.reactElement),
  };

  let make =
      (~dataSource, ~columns, ~rowKey=?, ~expandedRowRender=?, children) => {
    let props =
      props(
        ~dataSource,
        ~rowKey=Js.Nullable.fromOption(rowKey),
        ~expandedRowRender=Js.Nullable.fromOption(expandedRowRender),
        ~columns=
          Array.map(columns, item =>
            {
              "key": item##key,
              "dataIndex": Option.getWithDefault(rowKey, "id"),
              "title": item##title,
              "width": Js.Nullable.fromOption(item##width),
              "render": Js.Nullable.fromOption(item##render),
            }
          ),
      );
    ReasonReact.wrapJsForReason(~reactClass=component, ~props, children);
  };
};