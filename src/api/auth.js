// @flow

import jwtDecode from 'jwt-decode';

import { post } from '../api';

export const TOKEN_KEY = 'auth-token';

export const login = (email: string, password: string) =>
  post('/auth/login', { email, password }).then(response => {
    const decoded = jwtDecode(response.data.token);

    return {
      userId: decoded.userId,
      token: response.data.token
    };
  });
