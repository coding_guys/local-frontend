// @flow

import { get } from '../api';
import { type PacketSample } from '../domain/packets';
import { type PaginatedResponse } from '../utils/commonTypes';
import qs from 'query-string';

export type PacketSampleQuery = {
  page: number,
  entitiesPerPage: number,
  l4Type: ?string,
  l7Type: ?string,
  source: ?string,
  destination: ?string,
  packetSizeFrom: ?number,
  packetSizeTo: ?number
};

export const getPacketSamples: PacketSampleQuery => Promise<PaginatedResponse<PacketSample>> = query => {
  const queryParams = qs.stringify(query);
  console.log(queryParams);
  return get(`/samples?${queryParams}`).then(response => response.data);
};
