// @flow

import { TOKEN_KEY } from './auth.js';
import storage from '../storage.js';

// $FlowFixMe
export const API_URL: string = process.env.REACT_APP_API_URL;

const HEADERS = {
  'Content-Type': 'application/json',
  Accept: 'application/json'
};

export function fetch(path: string, args: Object, resourceType: 'json' | 'text' = 'json'): Promise<*> {
  let params = { ...args };

  const authToken = storage.get(TOKEN_KEY);

  if (authToken) {
    params.headers['Authorization'] = `Bearer ${authToken}`;
  }

  return window.fetch(`${API_URL}${path}`, params).then(res => {
    if ([200, 201].includes(res.status)) {
      return res.text().then(text => {
        if (!text) return Promise.resolve({ status: res.status });
        else {
          const data = resourceType === 'json' ? JSON.parse(text) : text;
          return Promise.resolve({ status: res.status, data });
        }
      });
    } else {
      return res.text().then(message => Promise.reject({ status: res.status, message }));
    }
  });
}

export function post(path: string, data: Object): Promise<*> {
  return fetch(path, {
    method: 'POST',
    headers: HEADERS,
    body: JSON.stringify(data)
  });
}

export function put(path: string, data: Object): Promise<*> {
  return fetch(path, {
    method: 'PUT',
    headers: HEADERS,
    body: JSON.stringify(data)
  });
}

export function get(path: string): Promise<*> {
  return fetch(path, {
    method: 'GET',
    headers: HEADERS
  });
}

export function remove(path: string): Promise<*> {
  return fetch(path, {
    method: 'DELETE',
    headers: HEADERS
  });
}
