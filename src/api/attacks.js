// @flow

import { get } from '../api';
import { type Attack } from '../domain/attacks';

export const getAttacks: () => Promise<Array<Attack>> = () => get('/attacks').then(response => response.data);
