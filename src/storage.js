// @flow

function save(key: string, value: string): void {
  window.localStorage.setItem(key, value);
}

function get(key: string): string {
  return window.localStorage.getItem(key);
}

function remove(key: string): void {
  window.localStorage.removeItem(key);
}

export default { save, get, remove };

