// @flow

import uuid from 'uuid/v4';

export function randomId(): string {
  return uuid();
}
