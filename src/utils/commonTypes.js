// @flow

export type Field = {
  name: string,
  value: string | number | boolean | ?number | ?string | ?boolean
};

export type PaginatedResponse<T> = {
  pageCount: number,
  entities: Array<T>
};
